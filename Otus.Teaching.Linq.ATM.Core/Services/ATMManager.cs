﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        /// <summary>
        /// 1 Вывод информации о заданном аккаунте по логину и паролю
        /// </summary>
        public User LogIn(string login, string password)
        {
            var user = Users.FirstOrDefault(x => x.Login == login & x.Password == password);
            return user;
        }
        /// <summary>
        /// 2 Вывод данных о всех счетах заданного пользователя
        /// </summary>
        public IEnumerable<Account> GetInfoAccounts(User user)
        {
            var accounts = Accounts.Where(x => x.UserId == user.Id);
            return accounts;
        }
        /// <summary>
        /// 3 Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        /// </summary>
        public IEnumerable<OperationsHistory> GetInfoAccountsDetail(IEnumerable<Account> accounts)
        {
            var detail = accounts.Join(History, x => x.Id, y => y.AccountId, (x, y) => y);
            return detail;
        }
        /// <summary>
        /// 4 Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
        /// </summary>
        public IEnumerable<AllOperationsByUser> GetAllOPeration(OperationType operation)
        {
            var allOperation = History.Where(x => x.OperationType == operation)
                .Join(Accounts, x => x.AccountId, y => y.Id, (x, y) => new AllOperationsByUser()
                {
                    OperationUser = Users.FirstOrDefault(user => user.Id == y.UserId),
                    History = x
                });
            return allOperation;
        }
        /// <summary>
        /// 5 Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        /// </summary>
        public IEnumerable<User> GetUsersByCash(int cash)
        {
            var allScoreUser = Accounts.Where(x => x.CashAll > cash).Join(Users, x => x.UserId, y => y.Id, (x, y) => y);
            return allScoreUser;
        }
        //TODO: Добавить методы получения данных для банкомата
    }
}