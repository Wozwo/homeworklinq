﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class AllOperationsByUser
    {
        public User OperationUser { get; set; }
        public OperationsHistory History { get; set; }
    }
}
