﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            //1. Вывод информации о заданном аккаунте по логину и паролю
            var user = atmManager.LogIn("snow", "111");
            if (user != null)
            {
                PrintATM($"Information by USER\nId: {user.Id}\n{user.FirstName} {user.SurName} {user.MiddleName}\n" +
                $"Passport: {user.PassportSeriesAndNumber}\nPhone: {user.Phone}\nRegistrationDate: {user.RegistrationDate}\n");
            }
            else
                PrintATM("Incorrectly entered login or password");
            //2. Вывод данных о всех счетах заданного пользователя
            if (user != null)
            {
                var accounts = atmManager.GetInfoAccounts(user);
                
                foreach (var account in accounts)
                {
                    PrintATM($"Information by Score\nId: {account.Id}\nUserId: {account.UserId}\nCashAll: {account.CashAll}\nOpeningDate: {account.OpeningDate}\n");
                }
            }
            //3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
            if (user != null)
            {
                var accounts = atmManager.GetInfoAccounts(user);
                var details = atmManager.GetInfoAccountsDetail(accounts);
                foreach (var account in accounts)
                {
                    PrintATM($"\nDetail information by Score: {account.Id}\nUserId: {account.UserId}\nCashAll: {account.CashAll}\nOpeningDate: {account.OpeningDate}");
                    foreach (var detail in details)
                    {
                        if (detail.AccountId == account.Id)
                            PrintATM($"   Id: {detail.Id}\n   AccountId :{detail.AccountId}\n   OperationDate: {detail.OperationDate}\n" +
                                $"   OperationType: {detail.OperationType}\n   CashSum: {detail.CashSum}");
                    }
                }
            }
            //4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
            if (user != null)
            {
                var historys = atmManager.GetAllOPeration(Core.Entities.OperationType.OutputCash);
                PrintATM($"\nHistory all OutputCash\n");
                foreach (var detail in historys)
                {
                    PrintATM($"Transactions\n" +
                        $"   Id: {detail.History.Id}\n" +
                        $"   AccountId :{detail.History.AccountId}\n" +
                        $"   OperationDate: {detail.History.OperationDate}\n" +
                        $"   OperationType: {detail.History.OperationType}\n" +
                        $"   CashSum: {detail.History.CashSum}\n" +
                        $"Information by USER: {detail.OperationUser.FirstName} {detail.OperationUser.SurName} {detail.OperationUser.MiddleName}\n" +
                        $"   PassportSeriesAndNumber: {detail.OperationUser.PassportSeriesAndNumber}\n" +
                        $"   Phone: {detail.OperationUser.Phone}\n");
                }
            }
            //5. Вывод данных о всех пользователях у которых на счёте сумма больше N 
            if (user != null)
            {
                var users = atmManager.GetUsersByCash(100500);
                foreach (var item in users)
                {
                    PrintATM($"CASH by USER \nId: {item.Id}\n{item.FirstName} {item.SurName} {item.MiddleName}\n" +
                        $"Passport: {item.PassportSeriesAndNumber}\nPhone: {item.Phone}\nRegistrationDate: {item.RegistrationDate}\n");
                }
            }
            //TODO: Далее выводим результаты разработанных LINQ запросов

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static void PrintATM(string information)
        {
            System.Console.WriteLine(information);
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}